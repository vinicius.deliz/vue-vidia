// import dependency to handle HTTP request to our back end
import axios from 'axios'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex);


//to handle state
const state = {
    entries: [],
}

//to handle state
const getters = {}

//to handle actions
const actions = {
    getData({ commit }) {
        axios.get('https://www.souvidia.com/api/v1/ecom/procedure/')
        .then(response => {
        commit('SET_DATA', response.data)
        })
    }
}

//to handle mutations
const mutations = {
    SET_DATA(state, entries) {
        state.entries = entries
    }
}

//export store module
export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})